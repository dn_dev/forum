﻿using System;
using System.Collections.Generic;

namespace Forum.Core.Models
{
    public class TopicComment
    {
        public int TopicCommentId { get; set; }

        public DateTime TopicCommentDateTime { get; set; }

        public byte[] TopicAttachment { get; set; }

        public string AttachmentMimeType { get; set; }

        public string TopicCommentMessage { get; set; }

        public int TopicId { get; set; }

        public int TopicStarterId { get; set; }

        public virtual Topic Topic { get; set; }

        public virtual TopicStarter TopicStarter { get; set; }
    }
}

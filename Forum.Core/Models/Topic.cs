﻿using System;

namespace Forum.Core.Models
{
    public class Topic
    {
        public int TopicId { get; set; }

        public string TopicName { get; set; }

        public string TopicMessage { get; set; }

        public int TopicStarterId { get; set; }

        public DateTime TopicDateTime { get; set; }

        public virtual TopicStarter TopicStarter { get; set; }
    }
}

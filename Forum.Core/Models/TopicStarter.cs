﻿using System.Collections.Generic;

namespace Forum.Core.Models
{
    public class TopicStarter
    {
        public int TopicStarterId { get; set; }

        public string TopicStarterNick { get; set; }

        public int ForumRoleId { get; set; }

        public int ForumRankId { get; set; }

        public byte[] Avatar { get; set; }

        public string ImageMimeType { get; set; }

        public string TopicStarterEmail { get; set; }

        public virtual ICollection<Topic> Topics { get; set; }
    }
}

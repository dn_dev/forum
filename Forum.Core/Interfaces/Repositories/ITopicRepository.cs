﻿using Forum.Core.Models;

namespace Forum.Core.Interfaces.Repositories
{
    public interface ITopicRepository : IRepositoryBase<Topic>
    {
        Topic Get(int id);
    }
}

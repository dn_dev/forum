﻿using Forum.Core.Models;

namespace Forum.Core.Interfaces.Repositories
{
    public interface ITopicStarterRepository : IRepositoryBase<TopicStarter>
    {
        TopicStarter Get(int id);
    }
}

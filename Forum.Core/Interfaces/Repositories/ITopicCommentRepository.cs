﻿using Forum.Core.Models;

namespace Forum.Core.Interfaces.Repositories
{
    public interface ITopicCommentRepository : IRepositoryBase<TopicComment>
    {
        TopicComment Get(int id);
    }
}

﻿using Forum.Core.Models;

namespace Forum.Core.Interfaces.Managers
{
    public interface ITopicStarterManager
    {
        TopicStarter[] GetAllTopicStarters();

        TopicStarter GetTopicStarterById(int topicStarterId);

        void AddTopicStarter(int forumRoleId, int forumRankId, byte[] avatar, string imageMimeType, string topicStarterNick, string topicStarterEmail);

        void EditTopicStarter(int topicStarterId, int forumRoleId, int forumRankId, byte[] avatar, string imageMimeType, string topicStarterName, string topicStarterEmail);

        void DeleteTopicStarter(int topicStarterId);
    }
}

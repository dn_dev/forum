﻿using System;
using Forum.Core.Models;

namespace Forum.Core.Interfaces.Managers
{
    public interface ITopicCommentManager
    {
        TopicComment[] GetAllComments();

        TopicComment GetTopicCommentById(int commentId);

        void AddTopicComment(DateTime topicCommentDateTime, byte[] topicAttachment, string attachmentMimeType,
            string topicCommentMessage, int topicId, int topicStarterId);

        void EditTopicComment(int topicCommentId, DateTime topicCommentDateTime, byte[] topicAttachment,
            string attachmentMimeType, string topicCommentMessage, int topicId, int topicStarterId);

        void DeleteTopicComment(int topicCommentId);
    }
}

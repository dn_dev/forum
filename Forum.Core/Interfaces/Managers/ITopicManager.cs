﻿using System;
using Forum.Core.Models;

namespace Forum.Core.Interfaces.Managers
{
    public interface ITopicManager
    {
        Topic[] GetAllTopics();

        Topic GetTopicById(int topicId);

        void AddTopic(string topicName, string topicMessage, int topicStarterId, DateTime topicDateTime);

        void EditTopic(int topicId, string topicName, string topicMessage, int topicStarterId, DateTime topicDateTime);

        void DeleteTopic(int topicId);
    }
}

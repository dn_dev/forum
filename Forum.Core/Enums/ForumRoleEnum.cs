﻿
namespace Forum.Core.Enums
{
    public enum ForumRoleEnum
    {
        Visitor,
        Topic_Starter,
        Moderator,
        Admin
    }
}

﻿
namespace Forum.Core.Enums
{
    public enum ForumRankEnum
    {
        Silent_Visitor,
        Communicative_Guest,
        Active_Member,
        Veteran,
        Legend
    }
}

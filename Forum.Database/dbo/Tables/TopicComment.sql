﻿CREATE TABLE [dbo].[TopicComment] (
    [TopicCommentId]       INT             IDENTITY (1, 1) NOT NULL,
    [TopicCommentDateTime] DATETIME        NULL,
    [TopicAttachment]      VARBINARY (MAX) NULL,
    [AttachmentMimeType]   VARCHAR (50)    NULL,
    [TopicId]              INT             NOT NULL,
    [TopicStarterId]       INT             NOT NULL,
    [TopicCommentMessage]  VARCHAR (MAX)   NULL,
    CONSTRAINT [PK_TopicComment_TopicCommentId] PRIMARY KEY CLUSTERED ([TopicCommentId] ASC),
    CONSTRAINT [FK_TopicComment_TopicId] FOREIGN KEY ([TopicId]) REFERENCES [dbo].[Topic] ([TopicId]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_TopicComment_TopicStarterId] FOREIGN KEY ([TopicStarterId]) REFERENCES [dbo].[TopicStarter] ([TopicStarterId])
);


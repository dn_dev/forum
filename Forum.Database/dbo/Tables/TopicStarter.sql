﻿CREATE TABLE [dbo].[TopicStarter] (
    [TopicStarterId]    INT             IDENTITY (1, 1) NOT NULL,
    [ForumRoleId]       INT             NOT NULL,
    [ForumRankId]       INT             NOT NULL,
    [Avatar]            VARBINARY (MAX) NULL,
    [ImageMimeType]     VARCHAR (50)    NULL,
    [TopicStarterNick]  VARCHAR (50)    NULL,
    [TopicStarterEmail] NVARCHAR (256)  NOT NULL,
    CONSTRAINT [PK_TopicStarter_TopicStarterId] PRIMARY KEY CLUSTERED ([TopicStarterId] ASC)
);


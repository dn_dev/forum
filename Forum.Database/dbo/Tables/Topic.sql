﻿CREATE TABLE [dbo].[Topic] (
    [TopicId]        INT            IDENTITY (1, 1) NOT NULL,
    [TopicName]      NVARCHAR (50)  NULL,
    [TopicMessage]   NVARCHAR (MAX) NULL,
    [TopicStarterId] INT            NOT NULL,
    [TopicDateTime]  DATETIME2 (7)  NULL,
    CONSTRAINT [PK_Topic_TopicId] PRIMARY KEY CLUSTERED ([TopicId] ASC),
    CONSTRAINT [FK_Topic_TopicStarterId] FOREIGN KEY ([TopicStarterId]) REFERENCES [dbo].[TopicStarter] ([TopicStarterId]) ON DELETE CASCADE ON UPDATE CASCADE
);


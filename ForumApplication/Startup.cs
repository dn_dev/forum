﻿using System.Reflection;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ForumApplication.Startup))]
namespace ForumApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            var builder = new ContainerBuilder();
            // Register MVC controllers.
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            Assembly dataAssembly = Assembly.Load("Forum.Data");
            Assembly businessAssembly = Assembly.Load("Forum.Business");

            builder.RegisterAssemblyTypes(dataAssembly)
                .Where(t => t.Name.EndsWith("UnitOfWork")).AsImplementedInterfaces().InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(dataAssembly)
                .Where(t => t.Name.EndsWith("Repository") || t.Name.EndsWith("DbContext"))
                .AsImplementedInterfaces().InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(businessAssembly)
                .Where(t => t.Name.EndsWith("Manager"))
                .AsImplementedInterfaces().InstancePerLifetimeScope();

            var container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

            app.UseAutofacMiddleware(container);
        }
    }
}

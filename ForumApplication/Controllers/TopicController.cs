﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Forum.Core.Interfaces.Managers;
using Forum.Core.Interfaces.UnitOfWork;
using Forum.Core.Models;
using ForumApplication.Models;
using Microsoft.Owin.Security.Provider;

namespace ForumApplication.Controllers
{
    public class TopicController : Controller
    {
        private readonly ITopicManager _topicManager;
        private readonly ITopicStarterManager _topicStarterManager;
        private readonly IUnitOfWork _unitOfWork;

        public TopicController(ITopicManager topicManager, ITopicStarterManager topicStarterManager, IUnitOfWork unitOfWork)
        {
            _topicManager = topicManager;
            _topicStarterManager = topicStarterManager;
            _unitOfWork = unitOfWork;
        }

        private TopicModel GetModel(int topicId)
        {
            var topic = _topicManager.GetTopicById(topicId);
            var model = new TopicModel()
            {
                TopicId = topic.TopicId,
                TopicMessage = topic.TopicMessage,
                TopicName = topic.TopicName,
                TopicDateTime = topic.TopicDateTime,
                TopicStarterId = topic.TopicStarterId
            };
            return model;
        }

        // GET: Topic
        public ActionResult Index()
        {
            var topics = _topicManager.GetAllTopics();
            return View(topics);
        }

        [Authorize]
        [HttpGet]
        public ActionResult AddNewTopic()
        {
            var topicStarter = _topicStarterManager.GetAllTopicStarters()
                .FirstOrDefault(x => x.TopicStarterEmail.Equals(User.Identity.Name, StringComparison.OrdinalIgnoreCase));
            var topic = new TopicModel();
            if (topicStarter != null)
            {
                ViewBag.IsAuthenticated = true;
                ViewBag.IsTopicStarter = true;
                topic.TopicStarterId = topicStarter.TopicStarterId;
            }
            return View("TopicDetails", topic);
        }

        [Authorize]
        [HttpPost]
        public ActionResult AddNewTopic(TopicModel topic)
        {
            if (ModelState.IsValid)
            {
                _topicManager.AddTopic(topic.TopicName, topic.TopicMessage, topic.TopicStarterId, DateTime.Now);
            }
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult EditTopic(int topicId)
        {
            var model = GetModel(topicId);
            ViewBag.IsAuthenticated = ControllerContext.HttpContext.Request.IsAuthenticated;
            ViewBag.IsTopicStarter = _topicStarterManager.GetTopicStarterById(model.TopicStarterId)
                .TopicStarterEmail == User.Identity.Name;
            return View("TopicDetails", model);
        }

        [HttpPost]
        public ActionResult EditTopic(TopicModel topicModel)
        {
            if (ModelState.IsValid)
            {
                _topicManager.EditTopic(topicModel.TopicId, topicModel.TopicName, topicModel.TopicMessage, topicModel.TopicStarterId, topicModel.TopicDateTime);
                _unitOfWork.Save();
            }
            return RedirectToAction("Index", "Home");
        }

        [Authorize]
        public ActionResult DeleteTopic(int topicId)
        {
            this._topicManager.DeleteTopic(topicId);
            return RedirectToAction("Index", "Home");
        }

        [Authorize]
        [HttpGet]
        public ActionResult CreateNewTopicComment(int topicId)
        {
            var currentTopicId = topicId;
            return RedirectToAction("AddNewTopicComment", "TopicComment", new { topicId = currentTopicId });
        }
    }
}
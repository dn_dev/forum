﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Forum.Core.Interfaces.Repositories;
using Forum.Core.Models;

namespace ForumApplication.Controllers
{
    public class HomeController : Controller
    {
        private readonly ITopicRepository _topicRepository;
        private readonly ITopicStarterRepository _topicStarterRepository;
        private readonly ITopicCommentRepository _topicCommentRepository;
        private readonly List<Topic> _topicsList;
        private readonly List<TopicComment> _commentsList;
        private readonly List<TopicStarter> _membersList;
        public HomeController(ITopicRepository topicRepository, 
            ITopicStarterRepository topicStarterRepository, 
            ITopicCommentRepository topicCommentRepository)
        {
            _topicRepository = topicRepository;
            _topicStarterRepository = topicStarterRepository;
            _topicCommentRepository = topicCommentRepository;
            _topicsList = topicRepository.SelectAll().ToList<Topic>();
            _commentsList = topicCommentRepository.SelectAll().ToList<TopicComment>();
            _membersList = topicStarterRepository.SelectAll().ToList<TopicStarter>();
        }

        public ActionResult Index()
        {
            ViewBag.IsAuthenticated = ControllerContext.RequestContext.HttpContext.Request.IsAuthenticated;
            if (_topicsList.Count != 0)
            { 
                var namesDict = new Dictionary<int,string>();
                var commentsQtyDict = new Dictionary<int, int>();
                var lastCommentDict = new Dictionary<int, string>();
                foreach (var topic in _topicsList)
                {
                    namesDict.Add(topic.TopicStarterId,_topicStarterRepository.Get(topic.TopicStarterId).TopicStarterNick);
                }
                ViewBag.NamesDict = namesDict;
                foreach (var topic in _topicsList)
                {
                    commentsQtyDict.Add(topic.TopicId, _commentsList.Count(tc => tc.TopicId == topic.TopicId));
                }
                ViewBag.CommentsQtyDict = commentsQtyDict;
                foreach (var topic in _topicsList)
                {
                    lastCommentDict.Add(topic.TopicId,
                        _commentsList.Where(cmt => cmt.TopicId == topic.TopicId).Max(c => c.TopicCommentDateTime)
                            .ToString("MM.dd.yyyy HH:mm"));
                }
                ViewBag.LastCommentDict = lastCommentDict;
                ViewBag.Members = _membersList;
                return View(_topicsList);
            }
            return View(_topicsList);
        }

        [Authorize]
        [HttpGet]
        public ActionResult CreateTopic()
        {
            return RedirectToAction("AddNewTopic", "Topic");
        }
    }
}
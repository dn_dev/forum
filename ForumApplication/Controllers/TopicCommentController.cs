﻿using System;
using System.Linq;
using System.Web.Mvc;
using Forum.Core.Interfaces.Managers;
using Forum.Core.Interfaces.UnitOfWork;
using Forum.Core.Models;
using ForumApplication.Models;

namespace ForumApplication.Controllers
{
    public class TopicCommentController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ITopicCommentManager _topicCommentManager;
        private readonly ITopicStarterManager _topicStarterManager;
        public TopicCommentController(IUnitOfWork unitOfWork, ITopicCommentManager topicCommentManager, ITopicStarterManager topicStarterManager)
        {
            _unitOfWork = unitOfWork;
            _topicCommentManager = topicCommentManager;
            _topicStarterManager = topicStarterManager;
        }
        // GET: TopicComment
        public ActionResult Index()
        {
            return View();
        }
        [Authorize]
        [HttpGet]
        public ActionResult AddNewTopicComment(int topicId)
        {
            var topicStarter = _topicStarterManager.GetAllTopicStarters()
                .FirstOrDefault(x => x.TopicStarterEmail.Equals(User.Identity.Name, StringComparison.OrdinalIgnoreCase));
            var topicStarterId = topicStarter.TopicStarterId;
            var model = new TopicCommentModel
            {
                TopicId = topicId,
                TopicStarterId = topicStarterId
            };
            return PartialView("TopicCommentDetails", model);
        }
        [Authorize]
        [HttpPost]
        public ActionResult AddNewTopicComment(TopicCommentModel model)
        {
            if (ModelState.IsValid)
            {
                _topicCommentManager.AddTopicComment(DateTime.Now, model.TopicAttachment, model.AttachmentMimeType,
                    model.TopicCommentMessage, model.TopicId, model.TopicStarterId);
                _unitOfWork.Save();
                return RedirectToAction("EditTopic", "Topic", new { topicId = model.TopicId });
            }
            return RedirectToAction("AddNewTopicComment", model);
        }

        [Authorize]
        [HttpGet]
        public ActionResult EditTopicComment(int topicCommentId)
        {
            var model = GetModel(topicCommentId);
            return PartialView("TopicCommentDetails", model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult EditTopicComment(TopicCommentModel model)
        {
            if (ModelState.IsValid)
            {
                _topicCommentManager.EditTopicComment(model.TopicCommentId, DateTime.Now, model.TopicAttachment,
                    model.AttachmentMimeType, model.TopicCommentMessage, model.TopicId, model.TopicStarterId);
                _unitOfWork.Save();
                return RedirectToAction("EditTopic", "Topic",new {topicId = model.TopicId });
            }
            return PartialView("TopicCommentDetails", model);
        }

        [HttpGet]
        public ActionResult GetTopicComments(int topicId)
        {
            TopicComment[] topicCommentsList = _topicCommentManager.GetAllComments().Where(x => x.TopicId == topicId).ToArray();
            ViewBag.CommentOwnerId = null;
            if (ControllerContext.HttpContext.Request.IsAuthenticated)
            {
                var topicStarter = _topicStarterManager.GetAllTopicStarters()
                    .FirstOrDefault(m => m.TopicStarterEmail == User.Identity.Name);
                if (topicStarter != null)
                {
                    ViewBag.CommentOwnerId = topicStarter.TopicStarterId;
                }
            }
            return PartialView(topicCommentsList);
        }

        private TopicCommentModel GetModel(int id)
        {
            var record = _topicCommentManager.GetTopicCommentById(id);
            var model = new TopicCommentModel
            {
                TopicId = record.TopicId,
                TopicCommentId = record.TopicCommentId,
                TopicCommentMessage = record.TopicCommentMessage,
                TopicStarterId = record.TopicStarterId
            };
            return model;
        }
    }
}
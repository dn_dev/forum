﻿using System.Web;
using System.Web.Mvc;
using Forum.Core.Enums;
using Forum.Core.Interfaces.Managers;
using ForumApplication.Models;

namespace ForumApplication.Controllers
{
    public class TopicStarterController : Controller
    {
        private readonly ITopicStarterManager _topicStarterManager;
        // GET: TopicStarter
        public TopicStarterController(ITopicStarterManager topicStarterManager)
        {
            _topicStarterManager = topicStarterManager;
        }
        public ActionResult Index()
        {
            return View();
        }

        private TopicStarterModel GetModel(int topicId)
        {
            var topicStarter = _topicStarterManager.GetTopicStarterById(topicId);
            var model = new TopicStarterModel()
            {
               TopicStarterNick = topicStarter.TopicStarterNick,
               Avatar = topicStarter.Avatar,
               TopicStarterEmail = topicStarter.TopicStarterEmail,
               TopicStarterId = topicStarter.TopicStarterId,
               ForumRankId = (ForumRankEnum)topicStarter.ForumRankId,
               ForumRoleId = (ForumRoleEnum)topicStarter.ForumRankId,
               ImageMimeType = topicStarter.ImageMimeType
            };
            return model;
        }

        [HttpGet]
        public ActionResult AddNewTopicStarter()
        {
            var topicStarter = new TopicStarterModel
            {
                TopicStarterEmail = User.Identity.Name
            };
            return View("TopicStarterDetails",topicStarter);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddNewTopicStarter(TopicStarterModel topicStarter, HttpPostedFileBase image = null)
        {
            if (ModelState.IsValid)
            {
                if (image != null)
                {
                    topicStarter.Avatar = new byte[image.ContentLength];
                    image.InputStream.Read(topicStarter.Avatar, 0, image.ContentLength);
                    topicStarter.ImageMimeType = image.ContentType;
                }
                var forumRoleId = (int) topicStarter.ForumRoleId;
                var forumRankId = (int) topicStarter.ForumRankId;
                _topicStarterManager.AddTopicStarter(forumRoleId, forumRankId,
                    topicStarter.Avatar, topicStarter.ImageMimeType, topicStarter.TopicStarterNick,topicStarter.TopicStarterEmail);
            }
            return RedirectToAction("Index", "Home");
        }

        public ActionResult GetAvatar(int topicStarterId)
        {
            var topicStarter = _topicStarterManager.GetTopicStarterById(topicStarterId);
            if (topicStarter != null)
            {
                return File(topicStarter.Avatar, topicStarter.ImageMimeType);
            }
            return null;
        }

        [HttpGet]
        public ActionResult GetUserDetails(int topicStarterId)
        {
            var model = GetModel(topicStarterId);
            return PartialView("UserDetails",model);
        }

    }
}
﻿using System;
using System.ComponentModel;
using System.Web.Mvc;

namespace ForumApplication.Models
{
    public class TopicCommentModel
    {
        public int TopicCommentId { get; set; }

        public DateTime TopicCommentDateTime { get; set; }

        public byte[] TopicAttachment { get; set; }

        public string AttachmentMimeType { get; set; }
        [AllowHtml]
        [DisplayName("Message")]
        public string TopicCommentMessage { get; set; }

        public int TopicId { get; set; }

        public int TopicStarterId { get; set; }
    }
}
﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ForumApplication.Models
{
    public class TopicModel
    {
        public int TopicId { get; set; }

        [Display(Name = "Title")]
        public string TopicName { get; set; }

        [AllowHtml]
        [Display(Name = "Message")]
        public string TopicMessage { get; set; }

        public int TopicStarterId { get; set; }

        public DateTime TopicDateTime { get; set; }
    }
}
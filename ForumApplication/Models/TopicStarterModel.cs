﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Forum.Core.Enums;

namespace ForumApplication.Models
{
    public class TopicStarterModel
    {
        [HiddenInput(DisplayValue = false)]
        public int TopicStarterId { get; set; }

        [DisplayName("Nickname")]
        [MaxLength(15,ErrorMessage = "The name must not exceed 15 characters")]
        public string TopicStarterNick { get; set; }

        [DisplayName("Forum Role")]
        public ForumRoleEnum ForumRoleId { get; set; }

        [DisplayName("Forum Rank")]
        public ForumRankEnum ForumRankId { get; set; }

        [DisplayName("E-mail")]
        public string TopicStarterEmail { get; set; }

        public byte[] Avatar { get; set; }

        public string ImageMimeType { get; set; }
    }
}
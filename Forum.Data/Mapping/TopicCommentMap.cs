﻿using System.Data.Entity.ModelConfiguration;
using Forum.Core.Models;

namespace Forum.Data.Mapping
{
    class TopicCommentMap : EntityTypeConfiguration<TopicComment>
    {
        public TopicCommentMap()
        {
            // Primary Key
            this.HasKey(x => x.TopicCommentId);

            // Propertries

            // Table & Column Mappings

            this.ToTable("TopicComment");

            // Navigation Property

            this.HasRequired(x => x.Topic);
            this.HasRequired(x => x.TopicStarter);
        }
    }
}

﻿using System.Data.Entity.ModelConfiguration;
using Forum.Core.Models;

namespace Forum.Data.Mapping
{
    public class TopicMap : EntityTypeConfiguration<Topic>
    {
        public TopicMap()
        {
            // Primary Key
            this.HasKey(x => x.TopicId);

            //Properties

            // Table & Column Mappings

            this.ToTable("Topic");


            // Navigation properties
            this.HasRequired(x => x.TopicStarter);
        }
    }
}

﻿using System.Data.Entity.ModelConfiguration;
using Forum.Core.Models;

namespace Forum.Data.Mapping
{
    public class TopicStarterMap : EntityTypeConfiguration<TopicStarter>
    {
        public TopicStarterMap()
        {
            // Primary key
            this.HasKey(x => x.TopicStarterId);

            // Properties

            this.ToTable("TopicStarter");

            // Navigation properties
            this.HasMany(x => x.Topics);
        }
    }
}

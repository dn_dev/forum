﻿using System.Linq;
using Forum.Core.Interfaces.Repositories;
using Forum.Core.Interfaces.UnitOfWork;
using Forum.Core.Models;

namespace Forum.Data.Repository
{
    public class TopicStarterRepository : RepositoryBase<TopicStarter>, ITopicStarterRepository
    {
        public TopicStarterRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public TopicStarter Get(int id)
        {
            return this.Select(x => x.TopicStarterId == id).FirstOrDefault();
        }
    }
}

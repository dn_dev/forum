﻿using System.Linq;
using Forum.Core.Interfaces.Repositories;
using Forum.Core.Interfaces.UnitOfWork;
using Forum.Core.Models;

namespace Forum.Data.Repository
{
    public class TopicCommentRepository : RepositoryBase<TopicComment>, ITopicCommentRepository
    {
        public TopicCommentRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public TopicComment Get(int id)
        {
            return this.Select(x => x.TopicCommentId == id).FirstOrDefault();
        }
    }
}

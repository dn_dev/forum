﻿using System.Linq;
using Forum.Core.Interfaces.Repositories;
using Forum.Core.Interfaces.UnitOfWork;
using Forum.Core.Models;

namespace Forum.Data.Repository
{
    public class TopicRepository : RepositoryBase<Topic>, ITopicRepository
    {
        public TopicRepository(IUnitOfWork unitOfWork) : base(unitOfWork)
        {
        }

        public Topic Get(int id)
        {
            return this.Select(x => x.TopicId == id).FirstOrDefault();
        }
    }
}

﻿using System.Data.Entity;
using Forum.Core.Interfaces;
using Forum.Data.Mapping;

namespace Forum.Data.Context
{
    public class ForumApplicationDbContext : DbContext, IForumApplicationDbContext
    {
        public ForumApplicationDbContext() : base ("name=ForumEntities")
        {
            //this.Database.Log = txt => System.Diagnostics.Debug.WriteLine(txt);

            //The Entity Framework provider type 'System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer'
            //for the 'System.Data.SqlClient' ADO.NET provider could not be loaded.
            //Make sure the provider assembly is available to the running application.
            //See http://go.microsoft.com/fwlink/?LinkId=260882 for more information.

            var ensureDLLIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

        public ForumApplicationDbContext(string connectionStringName) : base("name="+connectionStringName)
        {
            var ensureDLLIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<ForumApplicationDbContext>(null);

            modelBuilder.Configurations.Add(new TopicMap());
            modelBuilder.Configurations.Add(new TopicStarterMap());
            modelBuilder.Configurations.Add(new TopicCommentMap());
        }
    }
}

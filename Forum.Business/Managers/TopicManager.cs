﻿using System;
using System.Linq;
using Forum.Core.Interfaces.Managers;
using Forum.Core.Interfaces.Repositories;
using Forum.Core.Interfaces.UnitOfWork;
using Forum.Core.Models;

namespace Forum.Business.Managers
{
    public class TopicManager : ITopicManager
    {
        private readonly ITopicRepository _topicRepository;
        private readonly IUnitOfWork _unitOfWork;

        public TopicManager(IUnitOfWork unitOfWork, ITopicRepository topicRepository)
        {
            _topicRepository = topicRepository;
            _unitOfWork = unitOfWork;
        }

        public Topic[] GetAllTopics()
        {
            return _topicRepository.SelectAll().ToArray();
        }

        public Topic GetTopicById(int topicId)
        {
            return _topicRepository.Find(topicId);
        }

        public void AddTopic(string topicName, string topicMessage, int topicStarterId, DateTime topicDateTime)
        {
            var model = new Topic
            {
                TopicName = topicName,
                TopicMessage = topicMessage,
                TopicStarterId = topicStarterId,
                TopicDateTime = topicDateTime
            };
            _topicRepository.Insert(model);
            _unitOfWork.Save();
        }

        public void EditTopic(int topicId, string topicName, string topicMessage, int topicStarterId, DateTime topicDateTime)
        {
            var record = _topicRepository.Get(topicId);
            if (record != null)
            {
                record.TopicName = topicName;
                record.TopicMessage = topicMessage;
                record.TopicStarterId = topicStarterId;
                record.TopicDateTime = topicDateTime;
            }
            _unitOfWork.Save();
        }

        public void DeleteTopic(int topicId)
        {
            var record = _topicRepository.Get(topicId);
            if (record != null)
            {
                _topicRepository.Delete(record);
            }
            _unitOfWork.Save();
        }
    }
}

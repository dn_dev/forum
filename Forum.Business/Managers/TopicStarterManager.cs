﻿using System.Linq;
using Forum.Core.Interfaces.Managers;
using Forum.Core.Interfaces.Repositories;
using Forum.Core.Interfaces.UnitOfWork;
using Forum.Core.Models;

namespace Forum.Business.Managers
{
    public class TopicStarterManager : ITopicStarterManager
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ITopicStarterRepository _topicStarterRepository;

        public TopicStarterManager(IUnitOfWork unitOfWork, ITopicStarterRepository topicStarterRepository)
        {
            _unitOfWork = unitOfWork;
            _topicStarterRepository = topicStarterRepository;
        }

        public TopicStarter[] GetAllTopicStarters()
        {
            return _topicStarterRepository.SelectAll().ToArray();
        }

        public TopicStarter GetTopicStarterById(int topicStarterId)
        {
            return _topicStarterRepository.Find(topicStarterId);
        }

        public void AddTopicStarter(int forumRoleId, int forumRankId, byte[] avatar, string imageMimeType, string topicStarterNick, string topicStarterEmail)
        {
            var topicStarter = new TopicStarter()
            {
                Avatar = avatar,
                ForumRankId = forumRankId,
                ForumRoleId = forumRoleId,
                ImageMimeType = imageMimeType,
                TopicStarterNick = topicStarterNick,
                TopicStarterEmail = topicStarterEmail
            };
            _topicStarterRepository.Insert(topicStarter);
            _unitOfWork.Save();
        }

        public void EditTopicStarter(int topicStarterId, int forumRoleId, int forumRankId, byte[] avatar, string imageMimeType, string topicStarterNick, string topicStarterEmail)
        {
            var record = _topicStarterRepository.Get(topicStarterId);
            if (record != null)
            {
                record.ForumRankId = forumRankId;
                record.ForumRoleId = forumRoleId;
                record.Avatar = avatar;
                record.ImageMimeType = imageMimeType;
                record.TopicStarterNick = topicStarterNick;
                record.TopicStarterEmail = topicStarterEmail;
            }
            _unitOfWork.Save();
        }

        public void DeleteTopicStarter(int topicStarterId)
        {
            var record = _topicStarterRepository.Get(topicStarterId);
            if (record != null)
            {
                _topicStarterRepository.Delete(record);
                _unitOfWork.Save();
            }
        }
    }
}

﻿using System;
using System.Linq;
using Forum.Core.Interfaces.Managers;
using Forum.Core.Interfaces.Repositories;
using Forum.Core.Interfaces.UnitOfWork;
using Forum.Core.Models;

namespace Forum.Business.Managers
{
    public class TopicCommentManager : ITopicCommentManager
    {
        private readonly ITopicCommentRepository _topicCommentRepository;
        private readonly IUnitOfWork _unitOfWork;
        public TopicCommentManager(IUnitOfWork unitOfWork, ITopicCommentRepository topicCommentRepository)
        {
            _topicCommentRepository = topicCommentRepository;
            _unitOfWork = unitOfWork;
        }
        public TopicComment[] GetAllComments()
        {
            return _topicCommentRepository.SelectAll().ToArray();
        }

        public TopicComment GetTopicCommentById(int commentId)
        {
            return _topicCommentRepository.Find(commentId);
        }

        public void AddTopicComment(DateTime topicCommentDateTime, byte[] topicAttachment, string attachmentMimeType, string topicCommentMessage, int topicId, int topicStarterId)
        {
            var comment = new TopicComment()
            {
                TopicId = topicId,
                TopicAttachment = topicAttachment,
                AttachmentMimeType = attachmentMimeType,
                TopicCommentDateTime = topicCommentDateTime,
                TopicStarterId = topicStarterId,
                TopicCommentMessage = topicCommentMessage
            };
            _topicCommentRepository.Insert(comment);
            _unitOfWork.Save();
        }

        public void EditTopicComment(int topicCommentId, DateTime topicCommentDateTime, byte[] topicAttachment,
            string attachmentMimeType, string topicCommentMessage, int topicId, int topicStarterId)
        {
            var record = _topicCommentRepository.Get(topicCommentId);
            if (record != null)
            {
                record.AttachmentMimeType = attachmentMimeType;
                record.TopicCommentDateTime = topicCommentDateTime;
                record.TopicId = topicId;
                record.TopicStarterId = topicStarterId;
                record.TopicCommentMessage = topicCommentMessage;
                record.TopicCommentId = topicCommentId;
            }
            _unitOfWork.Save();
        }

        public void DeleteTopicComment(int topicCommentId)
        {
            var record = _topicCommentRepository.Get(topicCommentId);
            if (record != null)
            {
                _topicCommentRepository.Delete(record);
                _unitOfWork.Save();
            }
        }
    }
}
